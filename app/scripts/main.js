$(document).ready(function () {

  $(document).ready(function () {
    $('#rb-fullpage').fullpage({
      //Navigation
      //menu: '#myMenu',
      lockAnchors: false,
      anchors: ['pageOne', 'pageTwo', 'pageThree', 'pageFour', 'pageFive', 'pageSix', 'pageSeven', 'pageEight'],
      navigation: false,
      navigationPosition: 'right',
      navigationTooltips: ['pageOne', 'pageTwo', 'pageThree', 'pageFour', 'pageFive', 'pageSix', 'pageSeven', 'pageEight'],
      showActiveTooltip: false,
      slidesNavigation: false,
      slidesNavPosition: 'bottom',

      //Scrolling
      css3: true,
      scrollingSpeed: 400,
      autoScrolling: true,
      fitToSection: true,
      fitToSectionDelay: 1000,
      scrollBar: true,
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
      loopBottom: false,
      loopTop: false,
      loopHorizontal: true,
      continuousVertical: false,
      continuousHorizontal: false,
      scrollHorizontally: false,
      interlockedSlides: false,
      dragAndMove: false,
      offsetSections: false,
      resetSliders: false,
      fadingEffect: false,
      //normalScrollElements: '#element1, .element2',
      scrollOverflow: false,
      touchSensitivity: 15,
      normalScrollElementTouchThreshold: 5,
      bigSectionsDestination: null,

      //Accessibility
      keyboardScrolling: true,
      animateAnchor: true,
      recordHistory: true,

      //Design
      controlArrows: true,
      verticalCentered: true,
      sectionsColor: ['#fff'],
      paddingTop: '0px',
      paddingBottom: '0px',
      //fixedElements: '#myMenu, .footer',
      responsiveWidth: 1200,
      responsiveHeight: 0,
      responsiveSlides: false,
      parallax: true,
      parallaxKey: 'YWx2YXJvdHJpZ28uY29tXzlNZGNHRnlZV3hzWVhnPTFyRQ==',
      parallaxOptions: {
        type: 'reveal',
        percentage: 62,
        property: 'translate'
      },
      //Custom selectors
      sectionSelector: '.section',
      slideSelector: '.slide',

      lazyLoading: true,

      //events
      onLeave: function (index, nextIndex, direction) { },
      afterLoad: function (anchorLink, index) {
      },
      afterRender: function () {
        new WOW().init();
      },
      afterResize: function () { },
      afterResponsive: function (isResponsive) { },
      afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
      },
      onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) { }
    });
  });

  var sync1 = $('#sync1');
  var sync2 = $('#sync2');
  var slidesPerPage = 5; //globaly define number of elements per page
  var syncedSecondary = true;

  sync1.owlCarousel({
    items: 1,
    slideSpeed: 2000,
    nav: true,
    navText: ['<img src="../images/owl_left.png">', '<img src="../images/owl_right.png">'],
    navClass: ['owl-prev', 'owl-next'],
    autoplay: false,
    dots: false,
    loop: true,
    responsiveRefreshRate: 200
  }).on('changed.owl.carousel', syncPosition);

  sync2
    .on('initialized.owl.carousel', function () {
      sync2.find('.owl-item').eq(0).addClass('current');
    })
    .owlCarousel({
      items: slidesPerPage,
      dots: false,
      nav: false,
      smartSpeed: 200,
      slideSpeed: 500,
      slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
      responsiveRefreshRate: 100
    }).on('changed.owl.carousel', syncPosition2);

  function syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;

    //if you disable loop you have to comment this block
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - (el.item.count / 2) - .5);

    if (current < 0) {
      current = count;
    }
    if (current > count) {
      current = 0;
    }

    //end block

    sync2
      .find('.owl-item')
      .removeClass('current')
      .eq(current)
      .addClass('current');
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();

    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }

  sync2.on('click', '.owl-item', function (e) {
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });

  $('.rb-top').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    dots: false,
    autoplay: false
  });

  $('.rb-repair').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    dots: false,
    autoplay: false
  });

  $('.home-team__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    dots: false,
    autoplay: false
  });

  $('.rb-tips__cards').slick({
    arrows: true,
    nextArrow: '<img src="./images/icon_right.png" class="slick-next">',
    prevArrow: '<img src="./images/icon_left.png" class="slick-prev">',
    dots: false,
    autoplay: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });

  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: false,
    focusOnSelect: true
  });

  $('.rb-navigation__switcher').click(function () {
    $('.rb-navigation').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.rb-navigation__close').click(function () {
    $('.rb-navigation').slideToggle('fast', function () {
      // Animation complete.
    });
  });

  $('.rb-collapse__head').click(function () {
    $(this).next().slideToggle('fast', function () {
      $(this).prev().find('.rb-collapse__head-arrow').toggleClass('rb-collapse__head-arrow_rotated');
      $('.rb-tips__cards').slick('setPosition');
    });
  });

  $('.portfolio-card__more').click(function () {
    $(this).parent().parent().parent().next().slideToggle('fast', function () {
      $(this).parent().find('.portfolio-card__more').toggle();
    });
  });

  $('.portfolio-card__less').click(function () {
    $(this).parent().parent().parent().slideToggle('fast', function () {
      $(this).parent().find('.portfolio-card__more').toggle();
    });
  });

});